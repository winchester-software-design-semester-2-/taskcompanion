# README

## TaskCompanion
App that modulates the work to put in for a given assignment.
It will break down the work into healthy & manageable chunks over a weekly or monthly basis.
The user will have to confirm they have done their due daily work.

| Index | Feature |
| ------ | ------ |
| 1 | Schedule work by populating calendar with daily tasks |
| 2 | Remind the user of their tasks |
| 3 | Incentivise app interaction with manual task completion confirmation |
| 4 | Incentivise task completion with app rewards (points, avatars, etc.) |
| 5 | Premium or free accounts to engage a wide audience such as students with low income |

